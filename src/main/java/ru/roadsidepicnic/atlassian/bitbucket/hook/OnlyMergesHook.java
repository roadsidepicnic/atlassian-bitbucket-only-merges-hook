package ru.roadsidepicnic.atlassian.bitbucket.hook;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.commit.MinimalCommit;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.bitbucket.util.PagedIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.*;


public class OnlyMergesHook implements PreReceiveRepositoryHook {
    private static final Logger log = LogManager.getLogger(OnlyMergesHook.class);
    private static final PageRequestImpl PAGE_REQUEST = new PageRequestImpl(0, 100);

    private final RefService refService;
    private final CommitService commitService;

    public OnlyMergesHook(RefService refService, CommitService commitService) {
        this.commitService = commitService;
        this.refService = refService;
    }

    /**
     * Disables regular commits on branches
     */
    @Override
    public boolean onReceive(
            @Nonnull RepositoryHookContext context,
            @Nonnull Collection<RefChange> refChanges,
            @Nonnull HookResponse hookResponse
    ) {
        Repository repository = context.getRepository();

        String[] refIds = ObjectUtils.firstNonNull(context.getSettings().getString("refs"), "").trim().split("\\s+");

        if (refIds.length == 0) {
            return true;
        }

        HashSet<String> refsToCheck = new HashSet<>();
        for (String refId : refIds) {
            if (refService.resolveRef(context.getRepository(), refId) != null) {
                refsToCheck.add(refId);
            }
        }

        ArrayList<String> errors = new ArrayList<>();

        for (RefChange refChange : refChanges) {
            if (refChange.getType() != RefChangeType.UPDATE || !refsToCheck.contains(refChange.getRef().getId())) {
                continue;
            }

            Vector<Commit> nonMergeCommits = findNonMergeCommits(repository, refChange);

            if (nonMergeCommits.size() > 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder
                        .append(String.format("The ref '%s' allows only merge commits.\n", refChange.getRef().getId()))
                        .append("But push contains regular commits:\n");

                for (Commit change : nonMergeCommits) {
                    stringBuilder.append(String.format("> %s %s\n", change.getDisplayId(), change.getMessage()));
                }
                errors.add(stringBuilder.toString());
            }
        }

        if (errors.size() > 0) {
            hookResponse.err().println(header());
            hookResponse.err().println(StringUtils.join(errors, "\n"));
            hookResponse.err().println(footer());

            return false;
        }

        return true;
    }

    private Vector<Commit> findNonMergeCommits(Repository repository, RefChange refChange) {
        Vector<Commit> changes = new Vector<>();
        for (Commit commit : getCommitsByFirstParent(repository, refChange))
            if (commit.getParents().size() <= 1) {
                changes.add(commit);
            }

        return changes;
    }

    private static String header() {
        return StringUtils.rightPad("", 100, "=") + "\n" +
            "   ___       _                                                             _    \n" +
            "  /___\\_ __ | |_   _    /\\/\\   ___ _ __ __ _  ___  ___    /\\  /\\___   ___ | | __\n" +
            " //  // '_ \\| | | | |  /    \\ / _ \\ '__/ _` |/ _ \\/ __|  / /_/ / _ \\ / _ \\| |/ /\n" +
            "/ \\_//| | | | | |_| | / /\\/\\ \\  __/ | | (_| |  __/\\__ \\ / __  / (_) | (_) |   < \n" +
            "\\___/ |_| |_|_|\\__, | \\/    \\/\\___|_|  \\__, |\\___||___/ \\/ /_/ \\___/ \\___/|_|\\_\\\n" +
            "               |___/                   |___/                                    \n" +
            StringUtils.leftPad("by Roadside Picnic (http://roadsidepicnic.ru)", 100, " ") + "\n"
        ;
    }

    private static String footer() {
        return StringUtils.rightPad("", 100, "=") + "\n";
    }

    public Iterable<Commit> getCommitsByFirstParent(Repository repository, RefChange refChange) {
        Map<String, Commit> commitMap = new HashMap<>();
        Set<Commit> commits = Sets.newHashSet();

        for (Commit commit : getPagedChanges(repository, refChange)) {
            commitMap.put(commit.getId(), commit);
            log.warn(String.format("commitMap: %s %s (%s)", commit.getId(), commit.getMessage(), commit.getParents().size()));
        }

        Commit currentCommit = commitMap.get(refChange.getToHash());

        while (currentCommit != null && !currentCommit.getId().equals(refChange.getFromHash())) {
            commits.add(currentCommit);

            MinimalCommit firstParent = Iterables.getFirst(currentCommit.getParents(), null);

            currentCommit = firstParent != null ? commitMap.get(firstParent.getId()) : null;
        }

        return commits;
    }

    public Iterable<Commit> getPagedChanges(final Repository repository, final RefChange refChange) {
        return new PagedIterable<>(pageRequest -> {
            return commitService.getCommitsBetween(
                    new CommitsBetweenRequest.Builder(repository)
                            .exclude(getBranchRefs(repository))
                            .include(refChange.getToHash())
                            .build(),
                    pageRequest
            );
        }, PAGE_REQUEST);
    }

    public Set<String> getBranchRefs(Repository repository) {
        Set<String> branchRefs = Sets.newHashSet();

        for (Branch branch : getBranches(repository)) {
            log.debug(String.format("getBranchRefs: branch found: %s", branch.toString()));
            if (branch.getId().startsWith("refs/heads/")) {
                branchRefs.add(branch.getId());
                log.debug(String.format("getBranchRefs: branch collected: %s", branch.toString()));
            }
        }

        return branchRefs;
    }

    public PagedIterable<Branch> getBranches(final Repository repository) {
        return new PagedIterable<>(pageRequest -> {
            return refService.getBranches(
                    new RepositoryBranchesRequest.Builder(repository).build(),
                    pageRequest
            );
        }, PAGE_REQUEST);
    }
}
